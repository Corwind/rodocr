

let max a b = if a>b then a else b


(* fonctions graphiques  *)


let myWidth img = (Sdlvideo.surface_info img).Sdlvideo.w

let myHeight img = (Sdlvideo.surface_info img).Sdlvideo.h


let getPixelColor img posX posY = Sdlvideo.get_pixel_color img posX posY

let isXCoordOk img posX = 
	if (posX < 0)||(posX > ((myWidth img)-1 ) ) then
		false
	else
		true
let isYCoordOk img posY = 
	if (posY < 0)||(posY > ((myHeight img)-1 ) ) then
		false
	else
		true
let areCoordOk img posX posY = ((isXCoordOk img posX)&&(isYCoordOk img posY))
	

let setPixelColor img posX posY color = 
	if areCoordOk img posX posY then
		Sdlvideo.put_pixel_color img posX posY color
	
let backgroundColor = (255,255,255);;



let isBlackLine img posY = 
	let blacka = ref false in
	for i = 0 to (myWidth img)-1 do 
		if (getPixelColor img i posY) <> backgroundColor then
			blacka := true;
	done;
	!blacka;;


let getBlackLines img =
	let blackLines = ref [] in
	for i = 0 to (myHeight img)-1 do
		if (isBlackLine img i) then
			blackLines := i::!blackLines;
	done;
	!blackLines;;


let hd l = 
	match l with
		(a::b) -> a
		|_ -> invalid_arg"je ne sais pas ce que je fais";;


let tl l = 
	match l with
		(a::b) -> b
		|_ -> invalid_arg"je ne sais pas ce que je fais";;


let reverseList l =
  let q = ref l and r = ref [] in
    while (!q <> []) do
      r := (hd !q) :: !r; q := tl !q
    done;
    !r;;


let rec getLength l =
	match l with
		[] -> 0
		|a::b -> 1 + getLength b ;;


let rec iEme l i = 
	match l with 
		[] -> invalid_arg"indice trop �lev�"
		|a::b when i = 1 -> a
		|a::b -> iEme b (i-1);;

let top (t,h) = t;;
let height (t,h) = h;;

let intermediaire l i firstline linelist = 
	linelist := (!firstline)::(!linelist);
	firstline := ( (iEme l i ),1);;


let getTxtLines l =
	let firstline = ref (-1,0) in
	let linelist = ref [] in
	for i = 1 to ( getLength l ) do
		if (!firstline) = (-1,0) then
 		firstline := ( (iEme l i), 1)
		else
			if (iEme l i) = (top !firstline) + (height !firstline) then
				firstline := ((top !firstline) , ((height !firstline) +1))
			else
				intermediaire l i firstline linelist;
			if i = getLength l then
				linelist := (!firstline)::(!linelist);
	done;
	!linelist;;

let rec isBlackColumn img (posX:int) (posY:int) (h:int) = 
	let isblack = ref false in
	for i = 1 to h do 
		if (getPixelColor img posX (posY + i - 1)) <> backgroundColor then
			isblack := true;
	done;
	!isblack;;


let getBlackColumns img (posY, h)=
	let blackColumns = ref [] in
	for posX = 1 to (myWidth img) do
		if (isBlackColumn img posX posY h) then
			blackColumns := (posX)::!blackColumns;
	done;
	(h, posY, !blackColumns);;


let isBlackCharLine img posX posY l =
	let blackli = ref false in
	for i = posX to posX+l-1 do
		if (getPixelColor img i posY) <> backgroundColor then
			blackli := true;
	done;
	!blackli;;

let rec getMinCharTop img ((posX,posY,l,h):int*int*int*int) = 
	if (isBlackCharLine img posX posY l) then
		(posX,posY,l,h)
	else
		getMinCharTop img (posX,posY+1,l,h-1);;

let rec getMinCharBot img ((posX,posY,l,h):int*int*int*int) = 
	if isBlackCharLine img posX (posY+h-1) l then
		(posX,posY,l,h)
	else
		getMinCharBot img (posX,posY,l,h-1);;

let getMinChar img ((posX,posY,l,h):int*int*int*int) = 
	getMinCharBot img (getMinCharTop img (posX,posY,l,h));;
	
let rec getMinCharPos img l = 
	match l with
		[] -> []
		|a::b -> (getMinChar img a)::(getMinCharPos img b)



let getTxtChar ((h:int), (posY:int) , (l:int list)) = 
	(h,posY, (getTxtLines (reverseList l)));;

let getCharPos ( (h:int), (posY:int), (l: (int*int)list)) = 
	let l2 = ref[] in 
	for i = (getLength l) downto 1 do
		l2 :=    (top(iEme l i),posY, height(iEme l i), h)::!l2 ;
	done;
	!l2;;

let getAllCharPos img listBlackLines = 
	let listlistposchar = ref [] in
	for i = 1 to getLength listBlackLines do
		listlistposchar := reverseList ((*!*)getMinCharPos img (*!*)(getCharPos( getTxtChar( getBlackColumns img (iEme listBlackLines i )))))::!listlistposchar ;
	done;
	reverseList !listlistposchar;;

let rec getLineTop l ymin = 
	match l with
		[] -> ymin
		|(xx,yy,ll,hh)::t when yy < ymin -> (getLineTop t yy)
		|e::t -> (getLineTop t ymin)

let rec getLineBot l yhmax = 
	match l with
		[] -> yhmax
		|(xx,yy,ll,hh)::t when yy+hh > yhmax -> (getLineBot t (yy+hh))
		|a::t -> (getLineBot t yhmax)

let getLineHeight l = (getLineBot l 0) - (getLineTop l 999999)

let hasEnoughSpace x1 l1 x2 h = (  x2 - (x1+l1) > (*!5*) h/3  )

let rec getLineSpaces (l:(int*int*int*int) list) lcst= 
	match l with 
		[] -> []
		|(a,b,c,d)::[] -> (a,b,c,d)::(a+c,b,-1,d)::[]
		|(a,b,c,d)::(e,f,g,h)::t when (hasEnoughSpace a c e (getLineHeight lcst)) -> (a,b,c,d)::(a+c,b,-1,d)::(getLineSpaces ((e,f,g,h)::t) lcst)
		|a::b -> a::(getLineSpaces b lcst)

let rec getAllSpaces l = 
	match l with
		[] -> []
		|a::b -> (getLineSpaces a a)::(getAllSpaces b)


let arrondi (n:float) =
	if ( n -. (float_of_int (int_of_float n)) ) > 0.5 then
		(int_of_float n) + 1
	else 
		(int_of_float n)

let debug_var tt ii = 
	print_string tt;
	print_int ii

let getMatrice (xx,yy,ll,hh) img = 
	let matrice_size = 9 in
	let mat = [| [| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |] |] in
	let size = (max ll hh) in
	let lineMat = ref 0 in
	let collumnMat = ref 0 in
	let lineChar = ref 0.0 in
	let collumnChar = ref 0.0 in
	let rate = (float_of_int size) /. (float_of_int matrice_size) in

	while ( !lineMat < matrice_size ) do
		collumnMat := 0;
		while( !collumnMat < matrice_size) do

			lineChar := (float_of_int (arrondi((float_of_int !lineMat) *.  rate)));
			while !lineChar < (float_of_int (arrondi( (float_of_int(!lineMat + 1))*. rate))) do

				collumnChar := (float_of_int (arrondi( (float_of_int !collumnMat) *. rate) ));
				while !collumnChar < (float_of_int (arrondi( (float_of_int (!collumnMat +1)) *. rate) )) do
					let px = (arrondi !collumnChar) in
					let py = (arrondi !lineChar) in
					if px < ll && py < hh then
					begin
						let pixel_color = getPixelColor img (px+xx) (py+yy) in
						if pixel_color <> backgroundColor then
							mat.(!lineMat).(!collumnMat) <- mat.(!lineMat).(!collumnMat) + 1;
					end;
					collumnChar := !collumnChar +. 1.0;
				done;
				lineChar := !lineChar +. 1.0;
			done;
			collumnMat := !collumnMat +1;
		done;	
		lineMat := !lineMat +1;
	done;
	mat


let getMatriceSmall (xx,yy,ll,hh) img = 
	let matrice_size = 9 in
	let mat = [| [| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |] |] in
	let size = (max ll hh) in
	let lineMat = ref 0 in
	let collumnMat = ref 0 in
	let lineChar = ref 0.0 in
	let collumnChar = ref 0.0 in
	let rate = (float_of_int matrice_size) /. (float_of_int size) in

	while !lineChar <  (float_of_int hh) do
		collumnChar := 0.0;
		while !collumnChar < (float_of_int ll) do

			lineMat := arrondi ( !lineChar *.  rate);
			while ( !lineMat < (arrondi ( (!lineChar +. 1.0) *.  rate) ) ) do

				collumnMat := (arrondi( !collumnChar *. rate) );
				while !collumnMat < (arrondi( (!collumnChar +. 1.0) *. rate) ) do

					let pixel_color = getPixelColor img ((int_of_float !collumnChar)+xx) ((int_of_float !lineChar)+yy) in
					if pixel_color <> backgroundColor then
						mat.(!lineMat).(!collumnMat) <- mat.(!lineMat).(!collumnMat) + 1;

					collumnMat := !collumnMat + 1;
				done;
				lineMat := !lineMat + 1;
			done;

			collumnChar := !collumnChar +. 1.0;
		done;	
		lineChar := !lineChar +. 1.0;
	done;
	mat


let printMatCle cle = 
	if cle = 0 then
		print_string"0"
	else
		(*print_int cle*)print_string"1"

let printMat matrice = 
	print_string"\n";
	for i = 0 to 8 do
		for j = 0 to 8 do
			printMatCle matrice.(i).(j);
		done;
		print_string"\n"
	done
let rec printLineMat l = 
	match l with 
		[] -> print_string"\nEndLine\n"
		|e::t -> (printMat e);(printLineMat t)
let rec printTextMat l = 
	match l with 
		[] -> print_string"\nEndText\n"
		|e::t -> (printLineMat e);(printTextMat t)


let getCharMatrice img (xx,yy,ll,hh) = 
	(*getMatrice (xx,yy,(max ll hh),(max ll hh)) (getCharImage img (xx,yy,ll,hh))*)
	if ll <> -1 then
		if (max ll hh) < 9 then
			getMatriceSmall (xx,yy,ll,hh) img
		else
			getMatrice (xx,yy,ll,hh) img
	else 
		[| [| 0; 0; 0; 0; 0; 0; 0; 0; 0 |];
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |]; 
		[| 0; 0; 0; 0; 0; 0; 0; 0; 0 |] |]

let rec getLineCharMatrice img l = 
	match l with
		[]->[]
		|e::t -> (getCharMatrice img  e)::(getLineCharMatrice img t)

let rec getTextCharMatrice img l = (*badass*)
	match l with 
		[] ->[]
		|e::t -> (getLineCharMatrice img e)::(getTextCharMatrice img t)


let matriceToArray mat = 
	let arr = Array.make 81 0 in
	for i = 0 to 80 do
		arr.(i) <- mat.(i/9).(i mod 9)
	done;
	arr

let arrayPourRaf1 = [|"a";"b";"c";"d";"e";"f";"g";"h";"i";"j";"k";"l";"m";"n";"o";"p";"q";"r";"s";"t";"u";"v";"w";"x";"y";"z"|]

let inarraytofloatarray arr = 
	let l = Array.length arr in
	let a = Array.make l 0.0 in
	for i = 0 to l - 1 do
		a.(i) <- (float_of_int arr.(i))
	done;
	a

let binarizeArray arr = 
	let length = Array.length arr in
	let arr2 = Array.make length 0.0 in
	for i = 0 to length - 1 do
		if arr.(i) > 0.0 then
			arr2.(i) <- 1.0
		else
			arr2.(i) <- 0.0
	done;
	arr2

let intMatListToFloatArrayArray l = 
	let length = getLength l in
	let arr = Array.make length ([|0.0|]) in
	for i = 1 to length do
		arr.(i-1) <- binarizeArray (inarraytofloatarray(matriceToArray(iEme l i)))
	done;
	arr


let getTestSet img = (* renvoie un tableau de tableau de float correspondant aux caract�res d'une image *)
	let matlist = hd (getTextCharMatrice img (getAllCharPos img (reverseList( getTxtLines (reverseList(getBlackLines img) ))))) in
	intMatListToFloatArrayArray matlist

let arrayPourRaf2 = 
	let fullArray = 
		[|
inarraytofloatarray[|0;1;1;1;1;1;0;0;0;1;1;0;0;1;1;1;0;0;1;1;0;0;1;1;1;0;0;0;0;1;1;1;1;1;0;0;0;1;1;0;1;1;1;0;0;1;1;0;0;1;1;1;0;0;1;1;0;0;1;1;1;0;0;1;1;0;1;1;1;1;1;0;1;1;1;1;1;1;1;1;0|];
inarraytofloatarray[|1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;1;1;1;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;1;1;1;0;0;0;0|];
inarraytofloatarray[|0;1;1;1;1;1;1;0;0;0;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;1;0;1;1;1;0;0;0;1;1;0;1;1;1;1;1;1;1;0;0;0;1;1;1;1;1;0;0;0|];
inarraytofloatarray[|0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;1;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;1;0;0;1;0;0;0;0;1;1;0;0;1;0;0;0;0;1;1;0;0;1;1;0;0;0;1;1;1;1;1;1;0;0;0|];
inarraytofloatarray[|0;1;1;1;1;1;1;0;0;0;1;0;0;1;1;1;0;0;1;1;0;0;0;1;1;1;0;1;1;1;1;1;1;1;1;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;1;0;1;1;1;0;0;0;1;1;0;0;1;1;1;1;1;1;0;0;0;1;1;1;1;1;0;0;0|];
inarraytofloatarray[|0;1;1;1;1;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;0;0;0;1;1;0;1;1;0;0;0;0;1;1;0;0;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;1;1;1;0;0;0;0;0;1;1;1;1;1;1;0;0;0;1;1;0;0;0;1;0;0;0;1;0;0;0;0;1;0;0;0;1;1;1;1;1;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;1;1;0;1;1;0;0;1;0;0;1;0;1;1;0;0;1;0;0;1;0;1;1;0;0;1;0;0;1;0;1;1;0;0;1;0;0;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;1;0;0;1;1;1;1;0;1;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;1;1;1;1;0;1;1;1;1|];
inarraytofloatarray[|1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;1;1;1;0;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;1;1;1;1;1;0;0|];
inarraytofloatarray[|1;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0|];
inarraytofloatarray[|0;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0|];
inarraytofloatarray[|1;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;1;1;1;0;0;0;0;1;0;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;0;1;1;0;0;0;0;1;1;1;1;1;1;1;0;0|];
inarraytofloatarray[|1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0|];
inarraytofloatarray[|0;0;1;1;1;1;1;0;0;0;1;1;0;0;0;1;1;0;1;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;1;1;1;1;0;0;0;0;1;1;1;1;1;0;0;0;0;1;1;1;1;1;0;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;1;1;1;1;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;0;0;0;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;1;1;1;1;0;0;0;0;1;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0|];
inarraytofloatarray[|0;1;1;1;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;0;0;0;1;1;0;0;0;1;0;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;1;1;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;1;1;1;0;0;0|];
inarraytofloatarray[|1;1;1;0;1;1;1;0;0;1;1;1;1;1;1;1;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;1;1;1;1;1;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;0;0;0;0;1;0;0;0;1;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;1;1;1;1;0;0;0;1;0;0;1;1;1;0;0;0;1;0;0;0;1;1;0;0;0;1;1;1;1;1;0;0;0;0|];
inarraytofloatarray[|0;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;0;1;1;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;1;1;0;1;1;1;1;0;0;1;1;1;0;1;1;1;0;0;0;0;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;0;0;1;1;1;0;1;1;0;0;0;0;1;0;0;0;1;1;0;0;1;1;0;0;0;1;1;0;0;1;0;0;0;0;1;1;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;0;1;1;0;1;1;0;1;1;0;1;0;0;1;1;0;1;1;0;1;0;0;1;1;1;0;1;1;1;0;0;0;1;1;0;1;1;0;0;0;0;1;1;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;0;1;1;1;0;0;1;1;1;0;1;1;0;0;0;0;1;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;0;0;0;1;1;1;0;0;0;0;0;1;1;1;1;0;0;0;0;1;1;0;1;1;1;0;0;0;1;0;0;0;1;1;1;0;1;1;1;0;1;1;1;1;1|];
inarraytofloatarray[|1;1;1;0;1;1;0;0;0;0;1;1;0;1;1;0;0;0;0;1;1;0;1;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;0;0;0;0;0;0;0;1;1;0;0;0;0;0;1;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;0;0|];
inarraytofloatarray[|1;1;1;1;1;1;1;1;0;1;0;0;0;1;1;1;0;0;0;0;0;1;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;0;0;1;1;0;0;0;0;0;0;1;1;1;0;0;0;0;0;1;1;1;0;0;0;0;1;0;1;1;1;1;1;1;1;1;0|];
		|]in
	fullArray


let sdl_init () =
  begin
    Sdl.init [`EVERYTHING];
    Sdlevent.enable_events Sdlevent.all_events_mask;
  end




let coloreLine img y = 
	begin
		for x = 0 to (myWidth img)-1 do
			if ((getPixelColor img x y) = backgroundColor) then
				Sdlvideo.put_pixel_color img x y (255,0,0);
		done
	end
let rec coloreLines img (l:int list) = 
	begin
		match l with
			[] -> Sdlvideo.put_pixel_color img 10 10 (getPixelColor img 10 10)
			| a::b -> (coloreLine img a); coloreLines img b
	end

let coloreChar img (posX, posY, l, h)  = 
	begin
		if l <> -1 then
			for y = posY to posY+h-1 do
				for x = posX to posX+l-1 do
					(*if ( (x=posX) || (x = posX+l-1) || (y = posY) || (y = posY + h -1)) then*)
						if ((getPixelColor img x y) = backgroundColor) then
							Sdlvideo.put_pixel_color img x y (0,250,0);
				done
			done
		else
			for y = posY to posY+h-1 do
				for x = posX to posX+5(*!*)-1 do
					if ((getPixelColor img x y) = backgroundColor) then
						setPixelColor img x y (0,0,255);
				done
			done;
	end


let rec coloreChars img (l:(int*int*int*int) list) =
	begin
		match l with
			[] -> print_string ""
			| a::b -> coloreChar img a ; coloreChars img b
	end

let rec coloreAllChars img (l:(int*int*int*int) list list) =
	begin
		match l with
			[] -> print_string ""
			| a::b -> coloreChars img a ; coloreAllChars img b
	end

let printcharPos (a:(int*int*int*int)) = 
	match a with
		(a,b,c,d) when (c = -1) -> print_string " SPACE "
		|(a,b,c,d) -> print_string " (";print_int a ; print_string ",";print_int b ; print_string ",";print_int c ; print_string ",";print_int d ; print_string ") ";;

let rec printCharsPos (l:(int*int*int*int) list) =
	match l with
		[] -> print_string ""
		|a::b -> printcharPos a; printCharsPos b;;
let rec printAllCharsPos (l:(int*int*int*int) list list) =
	match l with
		[] -> print_string "\nEnd. "
		|a::b -> print_string "\n new Line \n" ;printCharsPos a; printAllCharsPos b;;



(*let printTab tab =
	for i = 0 to (Vector.vect_length tab) -1 do
		print_int tab.(i); print_string " "
	done

let printMat mat = 
	for i = 0 to (Vector.vect_length mat) -1 do
		printTab mat.(i); print_string "\n"
	done*)

let rec wait_key () =
  let e = Sdlevent.wait_event () in
    match e with
    Sdlevent.KEYDOWN _ -> ()
      | _ -> wait_key ()

let show img dst =
  let d = Sdlvideo.display_format img in
    Sdlvideo.blit_surface d dst ();
    Sdlvideo.flip dst

let getBaseName str = 
	begin
		let start = (String.rindex str '/')+1 in
		let length = String.length str - start in
		String.sub str start length
	end

let getImgName () = 
	begin
		if Array.length (Sys.argv) < 2 then
      			failwith "Il manque le nom de l'image !"
		else
			Sys.argv.(1);
	end

let main imgName = 
	begin
		sdl_init ();
		let affichage = true in

		if affichage then begin
		let img = Sdlloader.load_image imgName in
		let display = Sdlvideo.set_video_mode (myWidth img) (myHeight img) [`DOUBLEBUF] in
		show img display;
		wait_key ()
		end;

		let img2 = Sdlloader.load_image imgName in
		let listBlackPixelLine = reverseList(getBlackLines img2) in
		if affichage then begin
		coloreLines img2 listBlackPixelLine;
		(*Sdlvideo.save_BMP img2 ("imgout/lines_"^(getBaseName imgName));*)

		let display2 = Sdlvideo.set_video_mode (myWidth img2) (myHeight img2) [`DOUBLEBUF] in
		show img2 display2;
		wait_key ()
		end;

		let img3 = Sdlloader.load_image imgName in
		let listBlackTxtLines = reverseList( getTxtLines listBlackPixelLine) in
		let charList = getAllSpaces(getAllCharPos img3 listBlackTxtLines) in
		if affichage then begin
			coloreAllChars img3 charList;
			(*Sdlvideo.save_BMP img3 ("imgout/chars_"^(getBaseName imgName));*)

			let display3 = Sdlvideo.set_video_mode (myWidth img3) (myHeight img3) [`DOUBLEBUF] in
			show img3 display3;
			wait_key ()
		end;
		
		(*printAllCharsPos charList;*)

		let img4 = Sdlloader.load_image imgName in
		let mats = getTextCharMatrice img4 charList in
		printTextMat mats;
		(*printMat (getMatrice (0,0,100,100) img4 );
		printMat (getMatrice (0,50,100,100) img4 );*)

		exit 0
	end


let printFloatArray arr = 
	for i = 0 to (Array.length arr)- 1 do
		print_int (int_of_float arr.(i));
		print_string" ; "
	done

let mainTest imgName = 
	sdl_init ();
	let img = Sdlloader.load_image imgName in

	let floatArrayArray = getTestSet img in

	for i = 0 to (Array.length floatArrayArray) - 1 do
		printFloatArray floatArrayArray.(i);
		print_string"\n"
	done

let rec concatLists (listlist: int array array list list) (listfinal: int array array list) = (*appeler avec listfinal = [] *)
	match listlist with 
		[] -> listfinal
		|e::l -> concatLists l (List.append listfinal e)


let getFloatArrayArray (listListMat: int array array list list) =
	let listMat = (concatLists listListMat []) in
	let floatArrayArray = (intMatListToFloatArrayArray listMat) in
	floatArrayArray

let finalMain img = 
	let mats = getTextCharMatrice img (getAllSpaces(getAllCharPos img (reverseList( getTxtLines (reverseList(getBlackLines img))))))in
	let arr = getFloatArrayArray mats in 
	arr
	

let _ = (*main*)main (getImgName())

