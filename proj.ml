let get_dims img =
  ((Sdlvideo.surface_info img).Sdlvideo.w, (Sdlvideo.surface_info img).Sdlvideo.h)
 
let sdl_init () =
  begin
    Sdl.init [`EVERYTHING];
    Sdlevent.enable_events Sdlevent.all_events_mask;
  end

let getBlue (r,g,b) = b
 
let rec wait_key () =
  let e = Sdlevent.wait_event () in
    match e with
    Sdlevent.KEYDOWN _ -> ()
      | _ -> wait_key ()
 
let show img dst =
  let d = Sdlvideo.display_format img in
    Sdlvideo.blit_surface d dst ();
    Sdlvideo.flip dst


let level (r,g,b) =
  let rf = float_of_int r and gf = float_of_int g and bf = float_of_int b in
  (0.3 *. rf +. 0.59 *. gf +. 0.11 *. bf)/. 255.

let color2grey (r,g,b) = 
  let gris = int_of_float (level (r,g,b) *. 255.) in 
  (gris,gris,gris)




let image2grey src dst =  
    for x = 0 to (Sdlvideo.surface_info src).Sdlvideo.w - 1 do
     for y = 0 to (Sdlvideo.surface_info src).Sdlvideo.h - 1 do
       let gris = color2grey (Sdlvideo.get_pixel_color src x y)
       in Sdlvideo.put_pixel_color dst x y gris  
     done
    done


let treshold src =
  let tresh = ref 0 in
  for x = 0 to (Sdlvideo.surface_info src).Sdlvideo.w - 1 do
    for y = 0 to (Sdlvideo.surface_info src).Sdlvideo.h - 1 do
      let (gris,_,_) = color2grey (Sdlvideo.get_pixel_color src x y) in
      tresh := !tresh + gris;
    done;
  done;

 ((((!tresh / ((Sdlvideo.surface_info src).Sdlvideo.w * (Sdlvideo.surface_info src).Sdlvideo.h))) * 2) / 3) 
 

let binarisation src dst =
	let tresh = treshold(src) in
	for x = 0 to (Sdlvideo.surface_info src).Sdlvideo.w - 1 do
		for y = 0 to (Sdlvideo.surface_info src).Sdlvideo.h - 1 do
			if (getBlue(color2grey(Sdlvideo.get_pixel_color src x y))) < tresh then
				Sdlvideo.put_pixel_color dst x y (0,0,0)
			else
				Sdlvideo.put_pixel_color dst x y (255,255,255)
		done
	done

let getImgName () = 
	begin
		if Array.length (Sys.argv) < 2 then
      			failwith "Il manque le nom de l'image !"
		else
			Sys.argv.(1);
	end

let bruit_off src dst =

  let w = (Sdlvideo.surface_info src).Sdlvideo.w - 1 in
  let h = (Sdlvideo.surface_info src).Sdlvideo.h - 1 in
  let l = ref [] in
  for x = 0 to w do
    for y = 0 to h do
      begin
	l := [];
	if (Sdlvideo.get_pixel_color src x y <> (255,255,255)) then
	for i = (x - 1) to (x + 1)  do
	  for j = (y - 1) to (y + 1) do
	      if i >= 0 && i <= w then
		(if j >= 0 && j <=  h then (
		  let (gris,_,_) = (Sdlvideo.get_pixel_color src i j) in
		  l := gris :: !l))
	  done;
	done;
	
	if (Sdlvideo.get_pixel_color src x y <> (255,255,255)) then 
	  begin  
	    l := List.sort  (function i -> function j -> match (i,j) with
	      (i,j) when i < j -> 1
	    |(i,j) when i = j -> 0
	    |_ -> -1)
	      !l;
	    
	    
	    let a = List.nth !l (((List.length !l) / 2) - 1) in
	    Sdlvideo.put_pixel_color dst x y (a,a,a)
	  end 
      end; 
      
    done; 
  done
    
      

let main imgName =
  begin
    sdl_init ();
    let img = Sdlloader.load_image imgName in
    
    let (w,h) = get_dims img in
    let dst = Sdlvideo.create_RGB_surface_format img [] w h in
    let dst2 = Sdlvideo.create_RGB_surface_format img [] w h in
    let dst3 = Sdlvideo.create_RGB_surface_format img [] w h in

    let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in
  
   
    show img display;
    wait_key();
    print_string "tartiflette";
    Pervasives.flush_all();
    image2grey img dst;
    show dst display;
    wait_key();
    binarisation dst dst2;
    show dst2 display;
    wait_key();
    (*bruit_off dst2 dst3;
    show dst3 display;
    wait_key(); *)
    Sdlvideo.save_BMP dst "binarized.bmp";
    Sdl.quit();
  
  end
    
    
let _ = main (getImgName())
