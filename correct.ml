(* IMPORTDICO *)
let hd l = 
	match l with
		(a::b) -> a
		|_ -> invalid_arg"je ne sais pas ce que je fais";;
let tl l = 
	match l with
		(a::b) -> b
		|_ -> invalid_arg"je ne sais pas ce que je fais";;
let reverseList l =
  let q = ref l and r = ref [] in
    while (!q <> []) do
      r := (hd !q) :: !r; q := tl !q
    done;
    !r;;

let getunfullname str = 
	let l = String.length str in
	let newstr = (String.sub str 0 (l-1)) in 
	newstr

(*
let rec loadDico r = 
	try 
		let i = getunfullname(input_line r) in
		i::(loadDico r)
	with End_of_file -> []

let getDico dicoName = 
	let r = open_in dicoName in
	
	let dicoList = loadDico r in

	close_in r;
	let ret = reverseList (dicoList) in
	print_string " \nCHARGED\n ";
	ret
*)

let rec loadDico2 r tab = 
	try 
		let word = getunfullname(input_line r) in
		let length = String.length word in 
		tab.(length) <-  word::tab.(length);
		loadDico2 r tab
	with End_of_file -> tab

let getDico2 dicoName = 
	let r = open_in dicoName in
	let tabSize = 30 in
	let dicoTabBase = Array.make tabSize [] in

	let dicoTab = loadDico2 r dicoTabBase in

	close_in r;

	for i = 0 to tabSize-1 do
		dicoTab.(i) <- reverseList dicoTab.(i)
	done;
	print_string " \nLOADED\n ";
	dicoTab

(*
let rec printDico dico = 
	match dico with
		[] -> print_string"\n"
		|e::t -> print_string "\n"; print_string e
*)

(* SEARCHDICO *)

(*
let mainDico str = 
	let dico = getDico "dico.txt" in
	print_string " \nFINISHDICO\n ";
	let exists = searchWord str dico in 
	if exists then
		print_string (" Word "^str^" exists \n")
	else
		print_string (" Word "^str^" doesn't exists \n");
	exists
*)

let rec searchWord str dico = 
	match dico with
		[] -> false
		|e::t when (String.compare e str) = 0-> true
		(*|e::t when (String.compare e str) > 0 -> false   ### PB ACCENT ? COMPARE TAILLE ? *)
		|e::t -> searchWord str t

let rec searchWord2 str dico = 
	let length = String.length str in
	match dico.(length) with
		[] -> false
		|e::t when (String.compare e str) = 0-> true
		(*|e::t when (String.compare e str) > 0 -> false   ### PB ACCENT ? COMPARE TAILLE ? *)
		|e::t -> searchWord str t 

let mainDico2 str = 
	let dico = getDico2 "dico.txt" in
	print_string " \nFINISHDICO\n ";
	let exists = searchWord2 str dico in 
	if exists then
		print_string (" Word "^str^" exists \n")
	else
		print_string (" Word "^str^" doesn't exists \n");
	exists

(* DISTANCE *)
let mini a b c = min a (min b c)

let levenshteinDistance str1 str2 = 
	let length1 = String.length str1 in
	let length2 = String.length str2 in

	let mat = Array.make_matrix (length1 + 1) (length2 + 1) 0 in
	
	for i = 0 to length1 do
		mat.(i).(0) <- i
	done;
	for j = 0 to length2 do
		mat.(0).(j) <- j
	done;

	for j = 1 to length2 do
		for i = 1 to length1 do

			if str1.[i-1] = str2.[j-1] then
				mat.(i).(j) <- mat.(i-1).(j-1)
			else
				mat.(i).(j) <- mini (mat.(i-1).(j) + 1)  (mat.(i).(j-1) + 1)  (mat.(i-1).(j-1) + 1)
		done;
	done;
	mat.(length1).(length2)


let main (str1,str2) = 
	print_string "Distance ";
	print_string str1;
	print_string " ";
	print_string str2;
	print_string " = ";
	print_int (levenshteinDistance str1 str2);
	print_string"\n"

(* SUGGESTIONS *)
let doMatch2 str test = 
	if (levenshteinDistance str test) <= 1(* ! *) then
		true
	else
		false

(*
let doMatch str test = 
	let length = String.length str in
	let lengthtest = String.length test in
	if (lengthtest < length-1)&&(lengthtest > length+1) then
		false
	else
		if (levenshteinDistance str test) <= 1(* ! *) then
			true
		else
			false

let rec suggest str dico close = (* close = [] *)
	match dico with 
		[] -> close
		|e::t when doMatch str e -> suggest str t (e::close)
		|e::t -> suggest str t close

let chooseSuggestion suggestions = 
	match suggestions with 
		[] -> "42"
		| e::l -> e

let rec printSuggestions suggestions = 
	match suggestions with 
		[] -> print_string""
		|e::t -> print_string (e^" "); printSuggestions t

let aux str dico = print_string ("Word "^str^" doesn't exists. Did you mean : "); printSuggestions (suggest str dico []); print_string " ?\n"

let mainSuggest str = 
	let dico = getDico "dico.txt" in
	printDico dico;
	let exists = searchWord str dico in 
	if exists then
		print_string ("Word "^str^" exists \n")
	else
		(*print_string ("Word "^str^" doesn't exists. Did you mean "^(chooseSuggestion (suggest str dico []))^" ?\n" );*)
		aux str dico;
	exists
*)

(* CORRECTION *)
let rec getASuggestion2aux str dico = 
	match dico with 
		[] -> ""
		|e::t when doMatch2 str e -> e
		|e::t -> getASuggestion2aux str t

let rec getASuggestion2 str dico = 
	let length = String.length str in
	if length >= (Array.length dico) then
		str
	else
		begin
		let try1 = getASuggestion2aux str dico.(length) in
		if try1 = "" then
			begin

			let try2 = getASuggestion2aux str dico.(length+1) in
			if try2 = "" then
				begin

				let try3 = getASuggestion2aux str dico.(length-1) in
				if try3 = "" then
					str
				else
					try3
			
				end
			else
				try2

			end
		else
			try1
		end

let mainOptimalSuggestion str = 
	let dico = getDico2 "dico.txt" in
	if searchWord2 str dico then
		print_string ("Word : "^str^" exists.\n")
	else 
		print_string ("Word : "^str^" doesn't exists. Did you mean : "^(getASuggestion2 str dico)^" ?\n")
(*
let rec getASuggestion str dico = 
	match dico with 
		[] -> str
		|e::t when doMatch str e -> e
		|e::t -> getASuggestion str t

let machin str dico = 
	if searchWord str dico then
		str
	else 
		getASuggestion str dico

let mainCorrect tabStr = 
	let dico = getDico "dico.txt" in
	let lengthTab = Array.length tabStr in
	let strDeBG = ref "" in
	for i = 1 to lengthTab-1 do
		strDeBG := (!strDeBG ^ (machin tabStr.(i) dico) ^ " ")
	done;
	strDeBG := getunfullname !strDeBG;
	print_string (!strDeBG^"\n");
	!strDeBG
*)

(* MAIN *)
let getStrings () = 
	begin
		if Array.length (Sys.argv) < 3 then
      			failwith "A string is missing"
		else
			(Sys.argv.(1),Sys.argv.(2));
	end

let getString () = 
	begin
		if Array.length (Sys.argv) < 2 then
      			failwith "A string is missing"
		else
			Sys.argv.(1);
	end

let _ = mainOptimalSuggestion (getString())(*(mainCorrect Sys.argv)*) (*main (getStrings () )*) (*(mainSuggest(getString()))*)
