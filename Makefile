    ##############################################################
    ##		 _____           _  ____   _____ _____  	##
    ## 		|  __ \         | |/ __ \ / ____|  __ \ 	##
    ## 		| |__) |___   __| | |  | | |    | |__) |	##
    ## 		|  _  // _ \ / _` | |  | | |    |  _  / 	##
    ## 	 	| | \ \ (_) | (_| | |__| | |____| | \ \ 	##
    ##		|_|  \_\___/ \__,_|\____/ \_____|_|  \_\	##
    ##								##
    ##		InfoSPE project of EPITA student's		##
    ##								##
    ##		Programmed by:					##
    ##		Rafael "Yayg" Gozlan 				##
    ##		Guillaume "Corwin" Dore				##
    ##		Raphael "TescoOne" Chauveau			##
    ##		Sulivan "FneuFneu" Drouard			##
    ##								##
    ##############################################################

SRC = tools.ml char_extraction.ml network.ml main.ml

CAML = ocaml
CAMLOPT =ocamlopt
CAMLB =ocamlbuild
OCAMLFLAGS= -I +sdl -I +site-lib/sdl -I +thread
OCAMLTOP =ocamlmktop

LIBS = bigarray.cmxa sdl.cmxa sdlloader.cmxa

EXE = rodocr

rodot: main.ml 
	${CAMLOPT} $(OCAMLFLAGS) $(LIBS) -o ${EXE} ${SRC}

char: char_extraction.ml
	${CAMLOPT} ${OCAMLFLAGS} ${LIBS} -o char char_extraction.ml


loutre: proj.ml
	${CAMLOPT} ${OCAMLFLAGS} ${LIBS} -o binarisation tools.ml proj.ml

sandwich: rotation.ml
	${CAMLOPT} ${OCAMLFLAGS} ${LIBS} -o rotation rotation.ml

lolwat: gui.ml
	${CAMLOPT} ${OCAMLFLAGS} ${LIBS} -o gui gui.ml

network: main.ml
	${OCAMLTOP} ${OCAMLFLAGS} tools.ml network.ml main.ml -o network

correct: correct.ml 
	${CAMLOPT} $(OCAMLFLAGS) $(LIBS) -o correct correct.ml



debug: clean
	${OCAMLTOP} -thread tools.ml network.ml -o network.ml

#	${CAML} -init  tools.ml network.ml

clean::
	rm -f *~ *.o *.cm? *.core rodocr network char rotation lolwat.bmp binarisation binarized.bmp

